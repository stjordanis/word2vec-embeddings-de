FROM tensorflow/tensorflow:1.11.0-py3

## Set a default user. Available via runtime flag `--user docker`
RUN useradd docker \
	&& mkdir /home/docker \
	&& chown docker:docker /home/docker \
	&& addgroup docker staff

RUN apt-get update -qq \
 && apt-get install --no-install-recommends -y \
    # install essentials
    build-essential \
    g++ \
    git \
    openssh-client \
    # requirements for numpy
    libopenblas-base \
    liblapack3 \
    wget \
    locales \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

#RUN apt-get update \
#    	&& apt-get install -y libx11-dev libfreetype6-dev libpng-dev pkg-config liblapack3 \
#    	                        libpq-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev  \
#                                libssl-dev libffi-dev \
#                                libxslt1-dev zlib1g-dev libhdf5-dev libhdf5-serial-dev


#PYTHON REQUIREMENTS
RUN python3 -m pip install pip --upgrade
RUN python3 -m pip install wheel
ADD requirements.txt /home/docker/code/requirements.txt
RUN python3 -m pip install -r /home/docker/code/requirements.txt

#add code
ADD process_wikipedia.py /home/docker/code/process_wikipedia.py
ADD train_word2vec.sh /home/docker/code/train_word2vec.sh
ADD src /home/docker/code/src
ADD eval /home/docker/code/eval

#Custom Tensorflow ops
CMD "cd /home/docker/code/ && ./train_word2vec.sh"
