# Dockerized Training of word2vec Embeddings

# Purpose
Dockerized training of german word2vec embeddings using latest wikipedia corpus or other plain, preprocessed text files.

# Approach 
1. Download latest articles from german wiki
2. Convert them with gensim into text, single words seperated by space, without punctuation
3. Train word2vec and save the resulting embedding files in the mounted OUTPUTDIR

# Usage on AWS 
use AMI: Ubuntu Server 16.04 LTS (HVM), SSD Volume Type - ami-759bc50a
with SSD EBS attatched (for German wiki 100GB is needed)

1. `git clone https://gitlab.com/deepset-ai/open-source/word2vec-embeddings-de.git`
2. `cd word2vec-embeddings-de`
3. `./install_docker.sh`  
4. either pull the image with `sudo docker pull registry.gitlab.com/deepset-ai/open-source/word2vec-embeddings-de` 
or build it yourself with `sudo docker build -t w2vec-de .` and adjust image: w2vec-de in "docker-compose-wiki.yml"
5. Adjust the mounted volumes in "docker-compose-wiki.yml" to some empty folder on your host
6. Adjust config via environment variables in "docker-compose.yml"
7. `sudo docker-compose -f docker-compose-wiki.yml up`
8. After termination you will find your embeddings in the directory that you mounted in 5.

Training time: ~ 7h on an EC2 Instance of type m5.2xlarge

Download of Wikipedia dump sometimes (obeserverd 1/12 times)fails, a restart is needed.

# Config
You can configure the word2vec training via the env variables in `docker-compose-wiki.yml`.  


